class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.string :title
      t.text :decription
      t.string :image
      t.string :link

      t.timestamps null: false
    end
  end
end
