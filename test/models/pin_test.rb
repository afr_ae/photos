require 'test_helper'

class PinTest < ActiveSupport::TestCase
  test "Pin debe tener un nombre" do 
  	@pin = Pin.new({
  		title: ''
  	})
  	
  	assert !@pin.valid?, "Sin nombre no es valido"
  end
end

