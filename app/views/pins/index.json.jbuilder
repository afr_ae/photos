json.array!(@pins) do |pin|
  json.extract! pin, :id, :title, :decription, :image, :link
  json.url pin_url(pin, format: :json)
end
