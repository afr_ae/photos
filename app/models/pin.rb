class Pin < ActiveRecord::Base
	validates :title, presence: true
	validates :decription, presence: true
	validates :image, presence: true
	validates :link, presence: true

	mount_uploader :image, PinImageUploader

	belongs_to :user


end
