// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.

//= require jquery
//= require pinterest_grid 
//= require jquery_ujs
//= require turbolinks
//= require_tree .


// $(document).ready(function() {
// 	$('#pics').pinterest_grid({
// 		no_columns: 4,
// 		padding_x: 10,
// 		padding_y: 10,
// 		margin_bottom: 50,
// 		single_column_breakpoint: 800
// 	});
// });

var ready = function() {
	$('#pics').pinterest_grid({
		no_columns: 4,
		padding_x: 10,
		padding_y: 10,
		margin_bottom: 50,
		single_column_breakpoint: 550
	});
}
$(document).on('ready', ready);
$(document).on('page:load', ready);



